#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#define r 3
#define p 5

int main()
{   
    int i, j;
    int E[r], A[r], C[p][r], R[p][r], marked[p], t[r];
    char g[3];
    int count = 0;
    int f = 1;

    FILE* input;
    input = fopen("input.txt","r");
    if (input == NULL) {

        printf("Can't open the file\n");
        return EXIT_FAILURE;

    }
    else {
      
      for (i = 0; i < r; i++) {
          fscanf(input, "%d", &E[i]);
      }

      fgets(g, sizeof(g), input);
      for (i = 0; i < r; i++) {
          fscanf(input, "%d", &A[i]);
      }

      fgets(g, sizeof(g), input);
      for (i = 0; i < p; i++) {
        for (j = 0; j < r; j++) {
           fscanf(input, "%d", &C[i][j]);
          }
      }

      fgets(g, sizeof(g), input);
      for (i = 0; i < p; i++) {
        for (j = 0; j < r; j++) {
           fscanf(input, "%d", &R[i][j]);
          }
      }
      
      printf("E = ( ");
      for (i = 0; i < r; i++) {
          printf("%d ", E[i]);
      }
      printf(")");
      printf("\nA = ( ");
      for (i = 0; i < r; i++) {
          printf("%d ", A[i]);
      }
      printf(")");
      printf("\nC = \n");
      for (i = 0; i < p; i++) {
        for (j = 0; j < r; j++) {
          printf("%d ", C[i][j]);
        }
        printf("\n");
      }
      printf("R = \n");
      for (i = 0; i < p; i++) {
        for (j = 0; j < r; j++) {
          printf("%d ", R[i][j]);
        }
        printf("\n");
      }
    }

    for (i = 0; i < p; i++) {
          marked[i] = 0;
    }

    while (f == 1) {

      f = 0;
      for (i = 0; i < p; i++) {
        if (marked[i] == 0) {
          for (int k = 0; k < r; k++) {
            t[k] = A[k];
          }
          for (j=0; j < r; j++) {
            int comp = A[j] + C[i][j];
            if (R[i][j] <= comp) {
              t[j] += C[i][j];
              if (j == r-1) {
                for (int k = 0; k < r; k++) {
                  A[k] = t[k];
                  C[i][k] = 0;
                  }
                f = 1;
                marked[i] = 1;
              }
            }
            else
              break;
          }
        }
      }

    }
    
    for (i = 0; i < p; i++) {

      if (marked[i] == 0) {
        printf("Process # %d is deadlocked\n", i);
        count++;
        }

      }
      if (count == 0)
        printf("no deadlock\n");
      printf("total number of deadlocks = %d", count);
  
  return EXIT_SUCCESS;
}