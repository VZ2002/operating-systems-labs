#include<stdio.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>

#define N 5

pthread_t pth[N];

void* job(void *arg)
{
    pthread_t id = pthread_self();

    printf("\n Hello from thread %lu\n", id);

    return NULL;
}

int main()
{
    int i = 0;
    int error;
    
    while(i < N)
    {
        error = pthread_create(&(pth[i]), NULL, &job, NULL);
        if (error != 0)
            printf("\ncan't create thread");
        else
            printf("\n Creating thread %lu\n", pth[i]);

        i++;
    }

    sleep(5);
    
    return EXIT_SUCCESS;
}