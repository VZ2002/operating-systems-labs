#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

#define N 200

char buffer[N];
int producerSleeps, consumerSleeps, pos;

void* Produce(void* args){

  while(1){

    if(producerSleeps){
      continue;
    }

    if(pos == N){
      consumerSleeps = 0;
      producerSleeps = 1;
    }
    
    buffer[pos] = '+';
    pos++;
  }

  return NULL;
}

void* Consume(void* args){

  while(1){

    if(consumerSleeps){
      continue;
    }

    if(pos == 0){
      consumerSleeps = 1;
      producerSleeps = 0;
    }
    
    buffer[pos] = '-';
    pos--;
  }

  return NULL;
}


int main() {
  pthread_t thProducer, thConsumer;
  
  consumerSleeps = 1;
  producerSleeps = 0;

  pthread_create(&thProducer, NULL, &Produce, NULL);
  pthread_create(&thConsumer, NULL, &Consume, NULL);
  
  return EXIT_SUCCESS;
}