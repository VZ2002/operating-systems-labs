#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

void swap(int* a, int* b)
{
    int temp = *a;
    *a = *b;
    *b = temp;
}

void arrivalSort(int num, int *arrival, int *burst, int *id, int *compl, int *waiting,int *turnaround)
{
    for (int i = 0; i < num; i++) {
        for (int j = 0; j < num - i - 1; j++) {
            if (arrival[j] > arrival[j + 1]) {
                    swap(&arrival[j], &arrival[j + 1]);
                    swap(&burst[j], &burst[j + 1]);
                    swap(&id[j], &id[j + 1]);
                    swap(&compl[j], &compl[j + 1]);
                    swap(&waiting[j], &waiting[j + 1]);
                    swap(&turnaround[j], &turnaround[j + 1]);
            }
        }
    }
}

void compleTime(int num, int *arrival, int *burst, int *id, int *compl, int *waiting,int *turnaround)
{
    int t, v;
    compl[0] = arrival[0] + burst[0];
    turnaround[0] = compl[0] - arrival[0];
    waiting[0] = turnaround[0] - burst[0];
 
    for (int i = 1; i < num; i++) {
        t = compl[i - 1];
        int l = burst[i];
        for (int j = i; j < num; j++) {
            if (t >= arrival[j] && l >= burst[j]) {
                l = burst[j];
                v = j;
            }
        }
        compl[v] = t + burst[v];
        turnaround[v] = compl[v] - arrival[v];
        waiting[v] = turnaround[v] - burst[v];
            swap(&arrival[v], &arrival[i]);
            swap(&burst[v], &burst[i]);
            swap(&id[v], &id[i]);
            swap(&compl[v], &compl[i]);
            swap(&waiting[v], &waiting[i]);
            swap(&turnaround[v], &turnaround[i]);
    }
}

int main() {
  int n = 0;
  int *arrival;
  int *burst;
  int *id;
  int *compl;
  int *waiting;
  int *turnaround;
  printf("Pls enter numbre of processes:\n");
  scanf("%d", &n);
  
  arrival = (int*)malloc(n*sizeof(int));
  burst = (int*)malloc(n*sizeof(int));
  id = (int*)malloc(n*sizeof(int));
  compl = (int*)malloc(n*sizeof(int));
  waiting = (int*)malloc(n*sizeof(int));
  turnaround = (int*)malloc(n*sizeof(int));

  for(int i=0;i<n;++i){
    id[i] = i + 1;
  }
  printf("N = %d\n", n);

  for(int i=0;i<n;++i){
    int x;
    printf("=================================\n");
    printf("Process # %d\n", id[i]);
    printf("Arrival Time is ");
    scanf("%d", &x);
    arrival[i] = x;
    printf("Burst Time is ");
    scanf("%d", &x);
    burst[i] = x;
  }
  printf("\n");
  printf("Statistics\n");
  printf("============================================================\n");

  arrivalSort(n, arrival, burst, id, compl, waiting, turnaround);
  compleTime(n, arrival, burst, id, compl, waiting, turnaround);

  float avWaiting, avTurn;
  int sumWait,sumTurn;
  sumWait = 0;
  sumTurn = 0;
  for(int i=0;i<n;i++){
    sumWait = sumWait + waiting[i];
    sumTurn = sumTurn + turnaround[i];
  }
  avWaiting = sumWait / n;
  avTurn = sumTurn / n;

  printf("Process id\n");
  for(int i=0;i<n;i++){
    printf("%d\n", id[i]);
  }
  printf("=================================\n");

  printf("Arrival Time\n");
  for(int i=0;i<n;i++){
    printf("%d\n", arrival[i]);
  }
  printf("=================================\n");

  printf("Burst Time\n");
  for(int i=0;i<n;i++){
    printf("%d\n", burst[i]);
  }
  printf("=================================\n");

  printf("Completion Time\n");
  for(int i=0;i<n;i++){
    printf("%d\n", compl[i]);
  }
  printf("=================================\n");

  printf("Waiting Time\n");
  for(int i=0;i<n;i++){
    printf("%d\n", waiting[i]);
  }
  printf("=================================\n");

  printf("Turnaround Time\n");
  for(int i=0;i<n;i++){
    printf("%d\n", turnaround[i]);
  }
  printf("=================================\n");

  printf("Average: Waiting Time = %f, Turnarounf Time = %f", avWaiting, avTurn);
  
  return EXIT_SUCCESS;
}