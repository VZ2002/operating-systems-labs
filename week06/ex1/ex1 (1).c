#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>

int main() {
  int n = 0;
  int *arrival;
  int *burst;
  int *id;
  int *compl;
  int *waiting;
  int *turnaround;
  printf("Pls enter numbre of processes:\n");
  scanf("%d", &n);
  
  arrival = (int*)malloc(n*sizeof(int));
  burst = (int*)malloc(n*sizeof(int));
  id = (int*)malloc(n*sizeof(int));
  compl = (int*)malloc(n*sizeof(int));
  waiting = (int*)malloc(n*sizeof(int));
  turnaround = (int*)malloc(n*sizeof(int));

  for(int i=0;i<n;++i){
    id[i] = i + 1;
  }
  printf("N = %d\n", n);

  for(int i=0;i<n;++i){
    int x;
    printf("=================================\n");
    printf("Process # %d\n", id[i]);
    printf("Arrival Time is ");
    scanf("%d", &x);
    arrival[i] = x;
    printf("Burst Time is ");
    scanf("%d", &x);
    burst[i] = x;
  }
  printf("\n");
  printf("Statistics\n");
  printf("============================================================\n");

  for(int i=0; i < n; i++)
    for(int j = i + 1; j < n; j++)
      if(arrival[j] < arrival[i]){
        int x = arrival[j];
        arrival[j] = arrival[i];
        arrival[i] = x;
        x = burst[j];
        burst[j] = burst[i];
        burst[i] = x;
        x = id[j];
        id[j] = id[i];
        id[i] = x;
      }

  for(int i=0; i <n; i++){
    if(i == 0){
      compl[i] = arrival[i] + burst[i];
      waiting[i] = 0; 
      turnaround[i] = waiting[i] + burst[i];
    } else{
      compl[i] = compl[i-1] + burst[i];
      waiting[i] = compl[i-1] - arrival[i];
      turnaround[i] = waiting[i] + burst[i];
    }
  }

  float avWaiting, avTurn;
  int sumWait,sumTurn;
  sumWait = 0;
  sumTurn = 0;
  for(int i=0;i<n;i++){
    sumWait = sumWait + waiting[i];
    sumTurn = sumTurn + turnaround[i];
  }
  avWaiting = sumWait / n;
  avTurn = sumTurn / n;

  printf("Process id\n");
  for(int i=0;i<n;i++){
    printf("%d\n", id[i]);
  }
  printf("=================================\n");

  printf("Arrival Time\n");
  for(int i=0;i<n;i++){
    printf("%d\n", arrival[i]);
  }
  printf("=================================\n");

  printf("Burst Time\n");
  for(int i=0;i<n;i++){
    printf("%d\n", burst[i]);
  }
  printf("=================================\n");

  printf("Completion Time\n");
  for(int i=0;i<n;i++){
    printf("%d\n", compl[i]);
  }
  printf("=================================\n");

  printf("Waiting Time\n");
  for(int i=0;i<n;i++){
    printf("%d\n", waiting[i]);
  }
  printf("=================================\n");

  printf("Turnaround Time\n");
  for(int i=0;i<n;i++){
    printf("%d\n", turnaround[i]);
  }
  printf("=================================\n");

  printf("Average: Waiting Time = %f, Turnarounf Time = %f", avWaiting, avTurn);
  
  return EXIT_SUCCESS;
}