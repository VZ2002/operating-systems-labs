#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <stdlib.h>
 
int main()
{
  int n, count, j, t, rm, f, w_time=0, t_time = 0;
  int *arrival;
  int *burst;
  int *id;
  int *compl;
  int *waiting;
  int *turnaround;
  int quantum;
  int *rt;

  printf("Pls enter number of processes:\n");
  scanf("%d",&n);
  rm=n;
  printf("Pls enter quantum number:\n");
  scanf("%d",&quantum);

  arrival = (int*)malloc(n*sizeof(int));
  burst = (int*)malloc(n*sizeof(int));
  id = (int*)malloc(n*sizeof(int));
  compl = (int*)malloc(n*sizeof(int));
  waiting = (int*)malloc(n*sizeof(int));
  turnaround = (int*)malloc(n*sizeof(int));
  rt = (int*)malloc(n*sizeof(int));

  for(int i=0;i<n;++i){
    id[i] = i + 1;
  }
  printf("N = %d\n", n);

  for(int i=0;i<n;++i){

    int x;
    printf("=================================\n");
    printf("Process # %d\n", id[i]);
    printf("Arrival Time is ");
    scanf("%d", &x);
    arrival[i] = x;
    printf("Burst Time is ");
    scanf("%d", &x);
    burst[i] = x;

  }
  printf("\n");
  printf("Statistics\n");
  printf("============================================================\n");

  for(int i=0;i<n;i++){
    rt[i] = burst[i];
  }
  printf("Process id|Arrival Time|Burst Time|Completion Time|Turnaround Time|Waiting Time\n\n");

  for(t=0,count=0;rm!=0;)
  {

    if(rt[count] <= quantum && rt[count]>0)
    {
      t+=rt[count];
      rt[count]=0;
      f=1;
    }
    else if(rt[count]>0)
    {
      rt[count]-=quantum;
      t+=quantum;
    }

    if((rt[count] ==0) && (f==1))
    {
      rm--;
      printf("P[%d]\t|\t%d\t|\t%d\t|\t%d\t|\t%d\t|\t%d\n",count+1,arrival[count], burst[count],t,t-arrival[count],t-arrival[count]-burst[count]);
      w_time+=t-arrival[count]-burst[count];
      t_time+=t-arrival[count];
      f=0;
    }

    if(count==n-1)
      count=0;
    else if(arrival[count+1]<=t)
      count++;
    else
      count=0;
  }

  printf("\nAverage: Waiting Time = %f, Turnaround Time = %f", w_time*1.0/n, t_time*1.0/n);

  return EXIT_SUCCESS;
}