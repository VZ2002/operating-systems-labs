#include <stdio.h>

struct node{

  int value;
  struct node *next;

};

struct linkedList{

  struct node *next;

};

void insert_node(struct linkedList* head, struct node *newNode, int val){

  struct node* first = head->next;
  newNode->value = val;

  head->next = newNode;
  newNode->next = first;

}

void delete_node(struct linkedList* head, int val){

    struct node* lastOne = NULL;
    if(head->next == NULL){
        return;
    }

    struct node* newOne = head->next;

    while(newOne != NULL){
        if(newOne->value == val){
            if(lastOne == NULL){
                head->next = newOne->next;
            }
            else{
                lastOne->next = newOne->next;
            }
            return;
        }
        lastOne = newOne;
        newOne = newOne->next;
    }
}

void print_list(struct linkedList *l) {

    struct node *n = l->next;
    while (n->next != NULL) {
        printf("%d\n", n->value);
        n = n->next;
    }
    printf("%d\n", n->value);

}
int main() {
    struct linkedList list;
    struct node n1;
    struct node n2;
    struct node n3;
    n1.value = 1;
    n1.next = NULL;
    list.next = &n1;

    insert_node(&list, &n2, 2);
    insert_node(&list, &n3, 3);
    print_list(&list);
    printf("\n");

    delete_node(&list, 1);
    delete_node(&list, 2);
    print_list(&list);

    return 0;
}