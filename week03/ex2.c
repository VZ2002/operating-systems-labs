#include <stdio.h>
#define N 10
void bubleSort(int *array, int size);
void printArray(int *arr, int size);

int main()
{

    int arr[N] = {1,7,4,5,3,2,55,2,0,20};
    bubleSort(arr,N);
    printArray(arr,N);
    return 0;
}

void bubleSort(int *array, int size) {
    for(int i = 0; i < size; i++) {
        for(int j = 0; j < size - i - 1; j++) {
            if(array[j] > array[j+1]) {
                int val = array[j];
                array[j] = array[j+1];
                array[j+1] = val;
            }
        }
    }
}

void printArray(int *arr, int size) {
    for(int i=0; i < N; ++i){
        printf("%d\n", arr[i]);
    }
}