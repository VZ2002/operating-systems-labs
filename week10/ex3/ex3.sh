echo "vova zelenokor" > _ex3.txt
chmod a-x _ex3.txt 
ls -l >> ex3.txt
chmod u=rwx _ex3.txt
ls -l >> ex3.txt
chmod o=rwx _ex3.txt
ls -l >> ex3.txt
chmod g=u _ex3.txt
ls -l >> ex3.txt

echo "660 means that now user and group are granted with permissions to read and write the file, but no any permission to others." >> ex3.txt
echo "cos decimal 6 in 8-base means 110. 1(on) - read, 1(on) - write, 0(off) - execute. 0 means 000, no any permission." >> ex3.txt
echo "775 means that now user and group are granted with permissions to read, write and execute the file, but only read and execute permissions to others." >> ex3.txt
echo "cos decimal 7 in 8-base means 111. 1(on) - read, 1(on) - write, 1(on) - execute." >> ex3.txt
echo "decimal 5 in 8-base means 101. 1(on) - read, 0(off) - write, 1(on) - execute." >> ex3.txt
echo "777 means that now everyone are granted with permissions to read, write and execute the file." >> ex3.txt
echo "cos decimal 7 in 8-base means 111. 1(on) - read, 1(on) - write, 1(on) - execute." >> ex3.txt
echo "and it appplies to everyone: user, group and others." >> ex3.txt