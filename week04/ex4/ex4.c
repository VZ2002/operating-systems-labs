#include <stdio.h>
#include <sys/types.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>

#define N 200

int main(){
  char command[N];
  char *args[N];
  int size =0;
  bool f = true;
  printf("Enter any command:\n");
  while(f){
    scanf("%[^\n]%*c", command);
    char *token = strtok(command, " ");

    while(token!=NULL){
      args[size] = token;
      token = strtok(NULL, " ");
      size++;
    }

    args[size]= NULL;

    pid_t x = fork();  
    if (x==0){
      if(size==1){
        system(command);
        printf("Enter any command:\n");
      }else{
        execvp(args[0], args);
        printf("Enter any command:\n");
      }
    }

    for(int i=0; i<size+1; i++){
        args[i]=NULL;
    }

    size=0;
  }
}