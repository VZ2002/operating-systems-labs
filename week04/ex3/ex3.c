 #include <stdio.h>
 #include <unistd.h>
 #include <sys/types.h>
 #include <stdlib.h>
 #include <string.h>
 #include <stdbool.h>

#define N 10

int main() {
  
  char command[N];
  bool f = true;
  while(f){
  printf("Enter the command without parameters, such as:\n");
  printf("pwd, ls, top, pstree and so on:\n");
  fgets(command, N, stdin);
  system(command);
  }
  return EXIT_SUCCESS;
}