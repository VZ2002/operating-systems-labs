 #include <stdio.h>
 #include <unistd.h>
 #include <sys/types.h>
 #include <stdlib.h>

int main() {
  
  int n = 10;
  pid_t x = fork();

  if(x == 0){
    printf("Hello from child [%d - %d]\n", getpid(), n);
  } else if(x > 0) {
    printf("Hello from parent [%d - %d]\n", getpid(), n);
  } else return EXIT_FAILURE;

  return EXIT_SUCCESS;
}