#include <stdio.h>
#define N 7
/*
Length of your string must be N.
If it will be more - string will be cut to length N.
If less - program will do nothing and returns nothing.
*/

void reverse(char* st);

int main() {
    char str[N];
	printf("Enter your string.\nIt's length must be N.\nIf it will be more - string will be cut to length N.\nIf less - program will do nothing and returns nothing:\n");
	fgets(str, N+1, stdin);
	reverse(str);
	puts(str);
    return 0;
}
 
void reverse(char* st){
	for(int i=0;i<N/2;++i){
		char temp = st[i];
		st[i] = st[N-i-1];
		st[N-i-1] = temp;
	}
}