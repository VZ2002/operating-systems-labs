#include <stdio.h>
#include <limits.h>
#include <float.h>

int main(){
	int i = 2;
	float f = 3.5;
	double d = 4.6;
	printf("int: %i; size: %lu; max: %i\n", i, sizeof(i), INT_MAX);
	printf("float: %f; size: %lu; max: %f\n", f, sizeof(f), FLT_MAX);
	printf("double: %f; size: %lu; max: %f\n", d, sizeof(d), DBL_MAX);

	return 0;
}