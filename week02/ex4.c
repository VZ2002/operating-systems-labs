#include <stdio.h>

void swap(int *in1, int *in2);

int main(){
	int x,y;
	printf("Enter first int number:\n");
	scanf("%d", &x);
	printf("Enter second int number:\n");
	scanf("%d", &y);
	swap(&x,&y);
	printf("x = %d, y = %d\n", x, y);
	return 0;
}

void swap(int *in1, int *in2){
	int val = *in1;
	*in1 = *in2;
	*in2 = val;
}