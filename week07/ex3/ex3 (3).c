#include <stdlib.h>
#include <stdio.h>


int main(){

	printf("Enter original array size:");
	int n1=0;
	scanf("%d",&n1);

	int* a1 = (int*)malloc(n1*sizeof(int));
	int i;
	for(i=0; i<n1; i++){
		a1[i] =100;
		printf("%d\n", a1[i]);
	}

	printf("\nEnter new array size: ");
	int n2=0;
	scanf("%d",&n2);

	//Dynamically change the array to size n2
	a1 = (int*)realloc(a1, n2);

	//If the new array is a larger size, set all new members to 0. Reason: dont want to use uninitialized variables.
  if(n2>n1)
    for(int i=0;i<n2;i++)
      a1[i] = 0;
	

	for(i=0; i<n2;i++){
		printf("%d\n",a1[i]);
	}
	printf("\n");
	
	return 0;
}