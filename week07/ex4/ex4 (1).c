#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void* new_realloc(void* ptr, int size, int newsize){
    if(ptr==NULL){
        return malloc(newsize);
    }
    else if(newsize == 0){
        free(ptr);
        return(NULL);
    }
    else{
        if(newsize > size){
            void* temp = malloc(newsize);
            memcpy(temp, ptr, size + sizeof(int));
            return temp;
        }
        else{
            void* temp = malloc(newsize);
            memcpy(temp, ptr, newsize + sizeof(int));
            return temp;
        }
    }
}

int main() {
  return EXIT_SUCCESS;
}