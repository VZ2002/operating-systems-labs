#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main() {
  int N;
  printf("Enter the number of integers in array:\n");
  scanf("%d", &N);
  int *array = (int*)malloc(N*sizeof(int));

  for(int i=0;i<N;i++)
    array[i] = i;

  for(int i=0;i<N;i++)
    printf("%d\n", array[i]);

  free(array);
  return EXIT_SUCCESS;
}