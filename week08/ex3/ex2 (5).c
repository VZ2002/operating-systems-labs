#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* For ex3:
 * From results we can conclude that: 
 * We doesnt use swap memory and in general doesnt use a lot of memory and CPU resources
*/
int main() {
char* ptr;
int size = 10*1024*1024; //10 MB

for(int i=0; i < 10; i++) {
  ptr = malloc(size); //number of bytes
  memset(ptr, 0, size);
  sleep(1);
  printf("Iter # %d\n", i);
}
// 10 MB = 10 * 1024
  return EXIT_SUCCESS;
}