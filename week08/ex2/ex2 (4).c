#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

/* From results we can conclude than: 
 * We have enough main memory because we ellocate not a big amount of memory for chars
 * we get zeroes in si and so that indicates that we dont use Swap
 * We have enough memory so no need to call swap and virtual memory
*/
int main() {
char* ptr;
int size = 10*1024*1024; //10 MB

for(int i=0; i < 10; i++) {
  ptr = malloc(size); //number of bytes
  memset(ptr, 0, size);
  sleep(1);
  printf("Iter # %d\n", i);
}
// 10 MB = 10 * 1024
  return EXIT_SUCCESS;
}