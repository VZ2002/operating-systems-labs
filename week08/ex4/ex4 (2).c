#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/resource.h> 

int main() {
char* ptr;
int size = 10*1024*1024; //10 MB

for(int i=0; i < 10; i++) {
  ptr = malloc(size); //number of bytes
  memset(ptr, 0, size);
  int who = RUSAGE_SELF; // RUSAGE_CHILDREN, RUSAGE_THREAD
  struct rusage u;
  int r = getrusage(who, &u);
  printf("Memory usage %lu\n", u.ru_maxrss);
  sleep(1);
  printf("Run # %d\n", i);
  free(ptr);
}
// 10 MB = 10 * 1024
  return EXIT_SUCCESS;
}